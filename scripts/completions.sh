#!/bin/sh
set -e
rm -rf completions
mkdir completions
for sh in bash zsh fish; do
    go run ./cmd/iam-oidc-tool completion "$sh" >"completions/iam-oidc-tool.$sh"
done
