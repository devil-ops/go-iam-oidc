# Go IDMS OIDC

 [![coverage report](https://gitlab.oit.duke.edu/devil-ops/go-iam-oidc/badges/main/coverage.svg)](https://gitlab.oit.duke.edu/devil-ops/go-iam-oidc/-/commits/main)

## Overview

Tools and libraries for dealing with getting a short lived token, from your long
lived token.

## External Documentation

* [Self Service OIDC](https://idms-web-selfservice.oit.duke.edu/advanced) - Register your token here
* [Official Docs](https://authentication.oit.duke.edu/manager/documentation/oauth/client_secrets.md) - From the maintainers of this service
* [Darins awesome project](https://gitlab.oit.duke.edu/londo003/idms-protected-service) - Describes how to interace with the service using shell or ruby
* [Jeremys awesome project](https://packrat.oit.duke.edu/api/v2/docs) - Example of how to use the information above in a real project

## Using the CLI Tool `iam-oidc-tool`

You can download this tool from the
[releases](https://gitlab.oit.duke.edu/devil-ops/go-iam-oidc/-/releases) page
above, or instally it by downloading this repository and building locally

Instead of passing your long lived token in as an argument, it is more secure to
use the environment variable: `IDMSOIDC_LONG_LIVED_TOKEN`

```console
$ iam-oidc-tool get-shortlived-token -h
A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.

Usage:
  iam-oidc-tool [command]

Available Commands:
  describe-longlived-token Get information on your long lived token
  get-shortlived-token     Get a short lived token from OIDC provider
  help                     Help about any command
  version                  Version will output the current build information

Flags:
      --config string   config file (default is $HOME/.iam-oidc-tool.yaml)
  -h, --help            help for iam-oidc-tool
  -t, --toggle          Help message for toggle

Use "iam-oidc-tool [command] --help" for more information about a command.
```

## Using the Go library

You can build the token getter in to your golang app using something similar to
the code below

```go
tokenInfo, _, err := authenticate.ShortLivedToken(
  &authenticate.ShortLivedTokenConfig{
    LongLivedToken: "long-lived-token",
    ClientID:       "my-client-id",
    Endpoints:      []string{"my-endpoint"},
  }
)
fmt.Println("Short Lived Token is: " tokenInfo.Token)
```
