---
image: gitlab-registry.oit.duke.edu/devil-ops/goreleaser:v2.2.0
default:
  retry:
    max: 2
    when: runner_system_failure
  tags:
    - oit-shared-unprivileged
variables:
  GOCACHE: /tmp/
  GOPATH: /tmp/
  GOLANGCI_LINT_CACHE: /tmp/
stages:
  - test
  - build
  - release
lint:
  stage: test
  script:
    - '[ -e .golangci.yml ] || cp /golangci/.golangci.yml .'
    - golangci-lint run --issues-exit-code 0 --out-format code-climate | tee gl-code-quality-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
  rules:
    - if: '$CI_COMMIT_TAG !~ /v[0-9]+\.[0-9]+\.[0-9]+/ '
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json
## Coverage will do visual coverage in the GitLab UI
coverage:
  stage: test
  coverage: '/total:.*\d+.\d+/'
  script:
    - go mod tidy
    - go test ./... -coverprofile=coverage-all.txt -covermode count
    ## EasyJson package is auto generated, no tests needed here in theory
    - grep -v _easyjson.go coverage-all.txt > coverage.txt
    - gocover-cobertura < coverage.txt > coverage.xml
    - go tool cover -func coverage.txt
  rules:
    - if: '$CI_COMMIT_TAG !~ /v[0-9]+\.[0-9]+\.[0-9]+/ '
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
## security looks for common security issues
security:
  stage: test
  allow_failure: true
  script:
    - govulncheck ./...
  rules:
    - if: '$CI_COMMIT_TAG !~ /v[0-9]+\.[0-9]+\.[0-9]+/ '
## check-releaser looks at some settings inside of the .goreleaser.yaml file and
## ensures that all the requirements are set correctly
check-release:
  stage: test
  variables:
    GIT_DEPTH: 0
  id_tokens:
    VAULT_ID_TOKEN:
      aud: https://vault-mgmt.oit.duke.edu
  script:
    - /scripts/packaging-validation.sh
    - eval $(/scripts/vault-env.sh ssi-systems-${CI_PROJECT_NAME}-packaging)
    - goreleaser check
  rules:
    - if: '$CI_COMMIT_TAG !~ /v[0-9]+\.[0-9]+\.[0-9]+/ '
release:
  stage: release
  id_tokens:
    VAULT_ID_TOKEN:
      aud: https://vault-mgmt.oit.duke.edu
  rules:
    - if: '$CI_COMMIT_TAG =~ /v[0-9]+\.[0-9]+\.[0-9]+/ '
  variables:
    GIT_DEPTH: 0
  script:
    - eval $(/scripts/vault-env.sh ssi-systems-${CI_PROJECT_NAME}-packaging)
    - goreleaser release --clean
