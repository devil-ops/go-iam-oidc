package authenticate

import "net/http"

// ErrorResponse represents a failed http response
type ErrorResponse struct {
	Message string `json:"errorMessage"`
	Error   bool   `json:"error"`
}

// TokenSuccessResponse holds the info from a successful token response
type TokenSuccessResponse struct {
	Error   bool `json:"error"`
	Results struct {
		Attributes struct {
			Token string `json:"accessToken"`
		} `json:"attributes"`
	} `json:"mapQueryResult"`
}

// Response contains the http Response object, along with any relevant metadata from a response
type Response struct {
	Response *http.Response
}

// RefreshResponse is the data we get back from a refresh request
type RefreshResponse struct {
	Error          bool           `json:"error,omitempty"`
	MapQueryResult MapQueryResult `json:"mapQueryResult,omitempty"`
}

// MapQueryResult is part of the RefreshResponse
type MapQueryResult struct {
	Attributes MapQueryResultAttributes `json:"attributes,omitempty"`
}

// MapQueryResultAttributes is the attributes of a MapQueryResult
type MapQueryResultAttributes struct {
	AccessToken  string `json:"accessToken,omitempty"`
	RefreshToken string `json:"refreshToken,omitempty"`
}
