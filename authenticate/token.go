package authenticate

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

// Tokener is an interface describing how a thing can get a short lived token
// using some config
type Tokener interface {
	// ShortLivedToken(*ShortLivedTokenConfig) (*TokenInfo, *Response, error)
	Token(*ShortLivedTokenConfig) (*TokenInfo, error)
}

// LongTokenClaims is information about the claims for a Long Lived Token
type LongTokenClaims struct {
	AuthorizedParty     string `json:"azp"`
	ExpirationTime      int    `json:"exp"`
	IssuedAt            int    `json:"iat"`
	Issuer              string `json:"iss"`
	JWTID               string `json:"jti"`
	Subject             string `json:"sub"`
	ClientIDRestriction string `json:"clientSecret_clientIdRestriction"`
}

// ShortTokenClaims is information about the claims for a Short Token
type ShortTokenClaims struct {
	AuthorizedParty       string `json:"azp"`
	ExpirationTime        int    `json:"exp"`
	IssuedAt              int    `json:"iat"`
	IsTokenAuthentication bool   `json:"isTokenAuthentication"`
	Issuer                string `json:"iss"`
	JWTID                 string `json:"jti"`
	Subject               string `json:"sub"`
}

// TokenInfo contains information about a given token
type TokenInfo struct {
	Token  string        `json:"token"`
	Claims jwt.MapClaims `json:"claims"`
}

// Expires Returns the time that the token expires
func (t TokenInfo) Expires() (*time.Time, error) {
	d, err := t.Claims.GetExpirationTime()
	if err != nil {
		return nil, err
	}

	if d == nil {
		return nil, errors.New("no expiration time listed")
	}
	u := d.Time.UTC()
	return &u, nil
	// Extract the claims
}

// ShortLivedToken uses a ShortLivedTokenConfig object to get a ShortLivedToken using the Built-In-Client
func ShortLivedToken(d *ShortLivedTokenConfig) (*TokenInfo, *Response, error) {
	return bic.ShortLivedToken(d)
}

// Token satisfies the Tokener interface requirements
func (c *Client) Token(stc *ShortLivedTokenConfig) (*TokenInfo, error) {
	ti, _, err := c.ShortLivedToken(stc)
	if err != nil {
		return nil, err
	}
	return ti, nil
}

// Refresh refreshes a Long Lived Token using a Refresh Token
func Refresh(s string) (*RefreshResponse, error) {
	return bic.Refresh(s)
}

// Refresh refreshes a Long Lived Token using a Refresh Token
func (c *Client) Refresh(s string) (*RefreshResponse, error) {
	rc, err := NewRefreshConfig(s)
	if err != nil {
		return nil, err
	}
	postData, err := json.Marshal(rc)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", c.refreshURL, bytes.NewBuffer(postData))
	if err != nil {
		return nil, err
	}

	var refreshResp RefreshResponse
	_, err = c.sendRequest(req, &refreshResp)
	if err != nil {
		return nil, err
	}

	if refreshResp.Error {
		return &refreshResp, errors.New("error refreshing token")
	}
	return &refreshResp, nil
}

// ShortLivedToken uses a ShortLivedTokenConfig object to get a ShortLivedToken
func (c *Client) ShortLivedToken(d *ShortLivedTokenConfig) (*TokenInfo, *Response, error) {
	// Verify the ClientID is set
	if d.ClientID == "" {
		return nil, nil, errors.New("must set ClientID")
	}
	// Verify we have at least 1 endpoint
	if len(d.Endpoints) == 0 {
		return nil, nil, errors.New("must add at least 1 item to Endpoints")
	}

	postData, err := json.Marshal(d)
	if err != nil {
		return nil, nil, err
	}
	req, err := http.NewRequest("POST", c.exchangeURL, bytes.NewBuffer(postData))
	if err != nil {
		return nil, nil, err
	}

	tokenResp := &TokenSuccessResponse{}
	resp, err := c.sendRequest(req, &tokenResp)
	if err != nil {
		return nil, resp, err
	}

	// var meta ShortTokenClaims
	meta, err := Decode(tokenResp.Results.Attributes.Token)
	if err != nil {
		return nil, nil, err
	}

	t := &TokenInfo{
		Token:  tokenResp.Results.Attributes.Token,
		Claims: meta,
	}
	return t, nil, nil
}

// Decode takes a string and returns the jwt.Claim
func Decode(token string) (jwt.MapClaims, error) {
	// Extract the jwt token claim/metadata here
	parser := jwt.Parser{}
	// ParseUnverified? Feels like we should be checking against a signature, but not sure what that is...
	tokenInfo, _, err := parser.ParseUnverified(token, jwt.MapClaims{})
	if err != nil {
		return nil, err
	}
	claims, ok := tokenInfo.Claims.(jwt.MapClaims)
	if !ok {
		return nil, fmt.Errorf("could not get token metadata")
	}

	return claims, nil
}
