/*
Package authenticate allows you to interact with the IDM OIDC service
Terminology:

	Long Lived Token (LLT): These are tokens you can request on
	https://idms-web-selfservice.oit.duke.edu/advanced. They live for a number of
	months.
	SLT - Short Lived Token: These are generated for a specific service using a
	Long Lived Token. They live for a number of seconds.
*/
package authenticate

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	"moul.io/http2curl"
)

// Client is the holder of all the interactive functions
type Client struct {
	client      *http.Client
	UserAgent   string
	exchangeURL string
	refreshURL  string
	printCurl   bool
}

// bic is the Built-In-Client. Use this for a more viper.Viper style interaction
var bic *Client

const (
	// exchangeURL is the actual Token Exchange URL
	exchangeURL string = "https://idms-web-ws.oit.duke.edu/idm-ws/clientSecret/createClientToken"
	// refreshURL is the url used for refreshing long lived tokens
	refreshURL string = "https://idms-web-ws.oit.duke.edu/idm-ws/clientSecret/refreshLongLivedToken"
)

func init() {
	bic = New()
}

// GetClient gets the Built-In-Client instance
func GetClient() *Client {
	return bic
}

// RefreshConfig contains the information needed to refresh your Long Lived Token
type RefreshConfig struct {
	RefreshToken string `json:"longLivedRefreshToken"`
}

// NewRefreshConfig returns a RefreshConfig, or errors if the token is empty
func NewRefreshConfig(s string) (*RefreshConfig, error) {
	if s == "" {
		return nil, errors.New("refresh token must not be empty")
	}
	return &RefreshConfig{
		RefreshToken: s,
	}, nil
}

// ShortLivedTokenConfig is the options needed for a Short Lived Token generation
type ShortLivedTokenConfig struct {
	LongLivedToken string   `json:"longLivedToken"`
	ClientID       string   `json:"clientId"`
	Endpoints      []string `json:"endpoints"`
}

// WithLLT sets a static long lived token
func WithLLT(s string) func(*ShortLivedTokenConfig) {
	return func(sltc *ShortLivedTokenConfig) {
		sltc.LongLivedToken = s
	}
}

// WithClientID sets the client ID
func WithClientID(s string) func(*ShortLivedTokenConfig) {
	return func(sltc *ShortLivedTokenConfig) {
		sltc.ClientID = s
	}
}

// WithEndpoints sets given endpoints in the SLT Config
func WithEndpoints(endpoints ...string) func(*ShortLivedTokenConfig) {
	return func(sltc *ShortLivedTokenConfig) {
		sltc.Endpoints = prefixEndpoints(endpoints)
	}
}

// WithPrintCurl prints a curl version of every request out
func WithPrintCurl() func(*Client) {
	return func(c *Client) {
		c.printCurl = true
	}
}

// WithLLTEnv loops through environment variables and sets the key to the first one it finds
func WithLLTEnv(envs ...string) func(*ShortLivedTokenConfig) {
	for _, env := range envs {
		g := os.Getenv(env)
		if g != "" {
			return func(sltc *ShortLivedTokenConfig) {
				sltc.LongLivedToken = g
			}
		}
	}
	return func(_ *ShortLivedTokenConfig) {}
}

// NewShortLivedTokenConfig is the generator fore new ShortLivedTokenConfig options
func NewShortLivedTokenConfig(opts ...func(*ShortLivedTokenConfig)) *ShortLivedTokenConfig {
	sltc := &ShortLivedTokenConfig{}
	for _, o := range opts {
		o(sltc)
	}
	return sltc
}

// WithUserAgent is used during client initialization and sets a custom UserAgent
func WithUserAgent(s string) func(*Client) {
	return func(c *Client) {
		c.UserAgent = s
	}
}

// WithExchangeURL is used during client initialization and sets a custom token exchange URL
func WithExchangeURL(s string) func(*Client) {
	if s != "" {
		return func(c *Client) {
			c.exchangeURL = s
		}
	}
	return func(*Client) {}
}

// WithRefreshURL is used during client initialization and sets a custom token refresh URL
func WithRefreshURL(s string) func(*Client) {
	if s != "" {
		return func(c *Client) {
			c.refreshURL = s
		}
	}
	return func(*Client) {}
}

// WithHTTPClient is used during client initialization and sets a custom http.Client
func WithHTTPClient(hc *http.Client) func(*Client) {
	return func(c *Client) {
		c.client = hc
	}
}

// New creates a new client with default options. Pass func(*Client) args in to add customizations
func New(options ...func(*Client)) *Client {
	c := &Client{
		exchangeURL: exchangeURL,
		refreshURL:  refreshURL,
		UserAgent:   "go-iam-oidc",
		client:      http.DefaultClient,
	}
	if os.Getenv("PRINT_CURL") != "" {
		options = append(options, WithPrintCurl())
	}
	for _, o := range options {
		o(c)
	}

	return c
}

func (c *Client) sendRequest(req *http.Request, v interface{}) (*Response, error) {
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")

	if c.printCurl {
		command, _ := http2curl.GetCurlCommand(req)
		fmt.Fprintf(os.Stderr, "%v\n", command)
	}

	res, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	defer dclose(res.Body)

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes ErrorResponse
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return nil, errors.New(errRes.Message)
		}

		return nil, fmt.Errorf("unknown error, status code: %d", res.StatusCode)
	}

	if err = json.NewDecoder(res.Body).Decode(&v); err != nil {
		return nil, err
	}
	r := &Response{Response: res}

	return r, nil
}

// ExchangeURL retrieves the Token Exchange URL that is currently set from the Built-In-Client
func ExchangeURL() string {
	return bic.ExchangeURL()
}

// ExchangeURL retrieves the Token Exchange URL that is currently set
func (c *Client) ExchangeURL() string { return c.exchangeURL }

// SetExchangeURL changes the URL  to a given string on the Built-In-Client
func SetExchangeURL(s string) { bic.SetExchangeURL(s) }

// SetExchangeURL changes the URL  to a given string
func (c *Client) SetExchangeURL(s string) {
	c.exchangeURL = s
}
