package authenticate

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	got := New()
	require.NotNil(t, got)
	require.Equal(t, "go-iam-oidc", got.UserAgent)

	got = New(WithUserAgent("foo"), WithHTTPClient(http.DefaultClient), WithExchangeURL("http://www.example.com/"))
	require.NotNil(t, got)
	require.Equal(t, "foo", got.UserAgent)
}

func TestEmptyExchangeURL(t *testing.T) {
	require.Equal(
		t,
		exchangeURL,
		New(WithExchangeURL("")).ExchangeURL(),
		"If given a am empty url, we should be getting the real one back",
	)
}

func TestGetBuiltin(t *testing.T) {
	got := GetClient()
	require.NotNil(t, got)
}

// TODREW: Fix these up with the new env method
func TestGetLLTWithEnv(t *testing.T) {
	// Test with a normal built in token
	config := NewShortLivedTokenConfig(WithLLT("foo"))
	require.Equal(t, "foo", config.LongLivedToken)

	// Test with an actual env var
	t.Setenv("TEST_OIDC_TOKEN", "test-oidc-from-env")
	config = NewShortLivedTokenConfig(WithLLTEnv("TEST_OIDC_TOKEN"))
	require.Equal(t, "test-oidc-from-env", config.LongLivedToken)

	// Test with NOTHING set
	config = NewShortLivedTokenConfig(WithLLTEnv("NEVER_EXISTING_ENV", "Another"))
	require.Equal(t, "", config.LongLivedToken)
}

func TestNewSLTConfig(t *testing.T) {
	got := NewShortLivedTokenConfig(
		WithEndpoints("foo"),
		WithClientID("my-client"),
	)
	require.Equal(t, []string{"endpoint:foo"}, got.Endpoints)
	require.Equal(t, "my-client", got.ClientID)
}

func TestErrorResponses(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(500)
	}))
	c := New(WithExchangeURL(tsrv.URL))
	got, _, err := c.ShortLivedToken(&ShortLivedTokenConfig{
		LongLivedToken: "foo",
		ClientID:       "some-client",
		Endpoints:      []string{"/bar"},
	})
	require.Error(t, err)
	require.Nil(t, got)
}

func TestRefresh(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(200)
		w.Write([]byte(`{"error": false, "mapQueryResult": {"attributes":{ "accessToken": "eyJraWQiOiJyc2ExIiwiYWxnIjoiUlMyNTYifQ.eyJzdWI-5kcG9pbnRSZXN0cmljdGlvblJlZ2V4IjoiLioiLCdlbWVudCIsImNsaW dFNlY3JldF9jbGllbnRJZFJlc3RyaWN0aW9uIjoiU2hpbxNTk3MTIyLCJqdGkiOiJiMGVmYmJjOS1lZTUwL RiNmMtYjBkZC1mNmVlYzVjNTI0MDYifQ.AUGYGPvkgWeWjvp_5LuSpUPsBqQ6chMUYFXlsemXVoEVfk99AgFfGUZyQtK6WS6KE6kPgPw", "refreshToken": "eyJhbGciOiJub25lIn0.eyJjbGllbnRTZWNyZXRRSZXN0cmljdGlvbiI6IlNoaWxlblRlc3Q3My03YjVjYTFkZTIwNDcifQ."}}}`))
	}))
	c := New(WithRefreshURL(tsrv.URL))
	got, err := c.Refresh("my-refresh-token")
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestNewRefershConfig(t *testing.T) {
	got, err := NewRefreshConfig("foo")
	require.NoError(t, err)
	require.NotNil(t, got)

	got, err = NewRefreshConfig("")
	require.Error(t, err)
	require.EqualError(t, err, "refresh token must not be empty")
	require.Nil(t, got)
}
