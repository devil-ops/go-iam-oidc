package authenticate

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

/*
func TestDecodeToken(t *testing.T) {
	var target *jwt.Token
	got := DecodeToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvcmxkIiwiaWF0IjoxNjcyMjQ1ODQ1fQ.GMPZqF3huI_J6kJEJRY44STEtr-ID8OXmMxtNb8jTuo", target)
	require.NoError(t, got)
}
*/

func TestDecode(t *testing.T) {
	got, err := Decode("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvcmxkIiwiaWF0IjoxNjcyMjQ1ODQ1fQ.GMPZqF3huI_J6kJEJRY44STEtr-ID8OXmMxtNb8jTuo")
	require.NoError(t, err)
	require.NotNil(t, got)
	// require.Equal(t, "foo", got)
}

func TestDecodeTokenInvalid(t *testing.T) {
	target, got := Decode("🤪")
	require.Error(t, got)
	require.Nil(t, target)
	require.EqualError(t, got, "token is malformed: token contains an invalid number of segments")
}

func TestShortLivedToken(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(200)
		// accessToken generated with:
		// cat testdata/jwt.plain.json | jwt encode --secret "fake" --exp="1728050509"
		w.Write([]byte(`{
			"error": false,
			"mapQueryResult": {
			  "attributes": {
			    "accessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MjgwNTA1MDksImlhdCI6MTcyODA1MDUxOX0.3lYBuQ2SSyFxorpB2ZVQyhKGpCmXb9ARSclV4CO7XuE"
			  }
			}
		      }`))
	}))

	// Test with a new client
	sct := &ShortLivedTokenConfig{
		LongLivedToken: "foo",
		ClientID:       "some-client",
		Endpoints:      []string{"/bar"},
	}
	c := New(WithExchangeURL(tsrv.URL))
	got, _, err := c.ShortLivedToken(sct)
	require.NoError(t, err)
	require.NotNil(t, got)

	expires, err := got.Expires()
	require.NoError(t, err)
	require.Equal(t, time.Date(2024, time.October, 4, 14, 1, 49, 0, time.UTC), *expires)

	// Also test the interface specific implementation
	gotS, err := c.Token(sct)
	require.NoError(t, err)
	require.NotEqual(t, "", gotS)

	// Test with a built in client
	SetExchangeURL(tsrv.URL)
	require.Equal(t, tsrv.URL, ExchangeURL())
	got, _, err = ShortLivedToken(&ShortLivedTokenConfig{
		LongLivedToken: "foo",
		ClientID:       "some-client",
		Endpoints:      []string{"/bar"},
	})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestInvalidSLTConfig(t *testing.T) {
	// Missing ClientID
	got, _, err := ShortLivedToken(&ShortLivedTokenConfig{
		LongLivedToken: "foo",
		Endpoints:      []string{"foo"},
	})
	require.Error(t, err)
	require.Nil(t, got)
	require.EqualError(t, err, "must set ClientID")

	// Empty Endpoints
	got, _, err = ShortLivedToken(&ShortLivedTokenConfig{
		ClientID:       "foo",
		LongLivedToken: "bar",
	})
	require.Error(t, err)
	require.Nil(t, got)
	require.EqualError(t, err, "must add at least 1 item to Endpoints")
}
