package authenticate

import (
	"fmt"
	"io"
	"strings"
)

// PrefixEndpoints adds a prefix of "endpoint:" to every string in a slice, if it does not already have that prefix
// Deprecated: Use prefixEndpoints, the public version will be removed soon
func PrefixEndpoints(ep []string) []string {
	return prefixEndpoints(ep)
}

func prefixEndpoints(ep []string) []string {
	endpoints := make([]string, len(ep))
	for idx, i := range ep {
		if strings.HasPrefix(i, "endpoint:") {
			endpoints[idx] = i
		} else {
			endpoints[idx] = fmt.Sprintf("endpoint:%s", i)
		}
	}
	return endpoints
}

func dclose(c io.Closer) {
	if err := c.Close(); err != nil {
		panic(err)
	}
}
