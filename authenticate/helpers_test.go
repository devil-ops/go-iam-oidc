package authenticate

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPrefixEndpoints(t *testing.T) {
	require.Equal(
		t,
		prefixEndpoints([]string{"foo", "bar"}),
		[]string{"endpoint:foo", "endpoint:bar"},
	)
	require.Equal(
		t,
		prefixEndpoints([]string{"foo", "endpoint:bar"}),
		[]string{"endpoint:foo", "endpoint:bar"},
	)
}
