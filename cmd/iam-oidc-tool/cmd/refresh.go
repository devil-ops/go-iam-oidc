package cmd

import (
	"errors"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/go-iam-oidc/authenticate"
)

// refreshCmd represents the refresh command
var refreshCmd = &cobra.Command{
	Use:   "refresh",
	Short: "Refresh your Long Lived Token using a Refresh Token",
	PreRun: func(cmd *cobra.Command, _ []string) {
		checkErr(viper.BindPFlag("refresh-token", cmd.PersistentFlags().Lookup("refresh-token")))
	},
	RunE: func(_ *cobra.Command, _ []string) error {
		rt := viper.GetString("refresh-token")
		if rt == "" {
			return errors.New("must set refresh-token")
		}
		resp, err := authenticate.Refresh(rt)
		if err != nil {
			return err
		}
		gout.MustPrint(resp)
		return nil
	},
}

func init() {
	rootCmd.AddCommand(refreshCmd)
	refreshCmd.PersistentFlags().StringP("refresh-token", "r", "", "Refresh Token")
}
