package cmd

import (
	"fmt"
	"io"
	"net/http"

	"github.com/charmbracelet/glamour"
	"github.com/spf13/cobra"
)

const docURL string = "https://authentication.oit.duke.edu/manager/documentation/oauth/client_secrets.md"

// documentationCmd represents the documentation command
var documentationCmd = &cobra.Command{
	Use:   "documentation",
	Short: "Read upstream documentation for the IAM OIDC pieces",
	RunE: func(_ *cobra.Command, _ []string) error {
		resp, err := http.Get(docURL)
		if err != nil {
			return err
		}
		defer dclose(resp.Body)

		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		doc, err := glamour.RenderBytes(body, "light")
		if err != nil {
			return err
		}
		fmt.Println(string(doc))
		return nil
	},
}

func init() {
	rootCmd.AddCommand(documentationCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// documentationCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// documentationCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
