package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/go-iam-oidc/authenticate"
)

// describeLonglivedToken represents the getShortlivedToken command
var describeLonglivedToken = &cobra.Command{
	Use:   "describe-longlived-token",
	Short: "Get information on your long lived token",
	Long:  `Decode a long lived token in to readable claims`,
	PreRunE: func(cmd *cobra.Command, _ []string) error {
		return viper.BindPFlag("long-lived-token", cmd.PersistentFlags().Lookup("long-lived-token"))
	},
	RunE: func(_ *cobra.Command, _ []string) error {
		longLivedToken := viper.GetString("long-lived-token")
		if longLivedToken == "" {
			return errors.New("must set long lived token, either through cmd or env var: IDMSOIDC_LONG_LIVED_TOKEN")
		}
		meta, err := authenticate.Decode(longLivedToken)
		if err != nil {
			return err
		}
		ret, err := json.Marshal(meta)
		if err != nil {
			return err
		}
		fmt.Println(string(ret))
		expF, ok := meta["exp"].(float64)
		if !ok {
			slog.Warn("could not convert exp in to float64")
		} else {
			expDate := time.Unix(int64(expF), 0)
			slog.Info("Token expires on", "date", expDate.Format("2006-01-02"), "days-left", time.Until(expDate).Hours()/24)
		}
		return nil
	},
}

func init() {
	rootCmd.AddCommand(describeLonglivedToken)
	describeLonglivedToken.PersistentFlags().StringP("long-lived-token", "l", "", "Long Lived Token (Can also be passed as env IDMSOIDC_LONG_LIVED_TOKEN)")
}
