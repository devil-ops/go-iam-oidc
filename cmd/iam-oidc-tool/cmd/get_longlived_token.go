package cmd

import (
	"fmt"

	"github.com/pkg/browser"
	"github.com/spf13/cobra"
)

const regURL string = "https://duke.is/getllt"

// getLongLivedTokenCmd represents the getLongLivedToken command
var getLongLivedTokenCmd = &cobra.Command{
	Use:     "get-longlived-token",
	Aliases: []string{"get-llt", "llt"},
	Short:   "Get a short lived token from OIDC provider",
	Long:    fmt.Sprintf(`This just opens a web browser directly to the page that handles the registration: %v`, regURL),
	RunE: func(cmd *cobra.Command, _ []string) error {
		if mustGetCmd[bool](*cmd, "open-browser") {
			fmt.Printf("Opening the following url for a new Long Lived Token:\n  %v\n", regURL)
			if err := browser.OpenURL(regURL); err != nil {
				return err
			}
		} else {
			fmt.Printf("Go to the following url for a new Long Lived Token:\n  %v\n", regURL)
		}
		return nil
	},
}

func init() {
	rootCmd.AddCommand(getLongLivedTokenCmd)
	getLongLivedTokenCmd.PersistentFlags().Bool("open-browser", true, "Open a browser to the registration URL")
}
