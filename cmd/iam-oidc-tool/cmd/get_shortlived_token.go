package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/go-iam-oidc/authenticate"
)

// getShortlivedTokenCmd represents the getShortlivedToken command
var getShortlivedTokenCmd = &cobra.Command{
	Use:     "get-shortlived-token",
	Aliases: []string{"slt", "get-slt"},
	Short:   "Get a short lived token from OIDC provider",
	Long: `Using the OIT IDMS Web Services, use your long lived token to get a short token
used for authenticating against another Duke service`,
	PreRunE: func(cmd *cobra.Command, _ []string) error {
		return viper.BindPFlag("long-lived-token", cmd.PersistentFlags().Lookup("long-lived-token"))
	},
	RunE: func(cmd *cobra.Command, _ []string) error {
		longLivedToken := viper.GetString("long-lived-token")
		if longLivedToken == "" {
			return errors.New("must set long lived token, either through cmd or env var: IDMSOIDC_LONG_LIVED_TOKEN")
		}
		clientID := mustGetCmd[string](*cmd, "client-id")
		endpoints := mustGetCmd[[]string](*cmd, "endpoints")
		tokenOnly := mustGetCmd[bool](*cmd, "token-only")

		tokenInfo, _, err := authenticate.ShortLivedToken(authenticate.NewShortLivedTokenConfig(
			authenticate.WithLLT(longLivedToken),
			authenticate.WithClientID(clientID),
			authenticate.WithEndpoints(endpoints...),
		))
		if err != nil {
			return err
		}

		// Finally do the output
		if tokenOnly {
			fmt.Print(tokenInfo.Token)
		} else {
			tokenJSON, err := json.Marshal(tokenInfo)
			if err != nil {
				return err
			}
			fmt.Println(string(tokenJSON))
			expires, err := tokenInfo.Expires()
			if err != nil {
				slog.Error("could not detect expiration", "err", err)
			} else {
				slog.Info("expiration", "time", expires, "in", -time.Since(*expires))
			}
		}
		return nil
	},
}

func init() {
	rootCmd.AddCommand(getShortlivedTokenCmd)
	getShortlivedTokenCmd.PersistentFlags().StringP("client-id", "c", "", "OIDC Client ID")
	if err := getShortlivedTokenCmd.MarkPersistentFlagRequired("client-id"); err != nil {
		panic(err)
	}
	getShortlivedTokenCmd.PersistentFlags().StringP("long-lived-token", "l", "", "Long Lived Token (Can also be passed as env IDMSOIDC_LONG_LIVED_TOKEN)")
	getShortlivedTokenCmd.PersistentFlags().StringSliceP("endpoints", "e", []string{}, "Endpoints")
	if err := getShortlivedTokenCmd.MarkPersistentFlagRequired("endpoints"); err != nil {
		panic(err)
	}
	getShortlivedTokenCmd.PersistentFlags().StringP("service-url", "s", "http://localhost:3000", "Service URL")
	getShortlivedTokenCmd.PersistentFlags().BoolP("token-only", "t", false, "Just output the token, no claims")
}
