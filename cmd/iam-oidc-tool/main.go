/*
Package main is the executable
*/
package main

import "gitlab.oit.duke.edu/devil-ops/go-iam-oidc/cmd/iam-oidc-tool/cmd"

func main() {
	cmd.Execute()
}
